﻿using UnityEngine;

namespace Camera
{
    public class CameraBehaviour : MonoBehaviour
    {
        //Target that my camera will follow
        [SerializeField] private Transform target;
        //Distance camera of target
        [SerializeField] private Vector3 offset;
        //Percent position of the camera to the target
        [SerializeField] private float smoothSpeed;


        // LateUpdate is called frame late in relation through real frame
        void FixedUpdate()
        {
            //position of my target + Distance of camera
            Vector3 desiredPosition = target.position + offset;            
            //Lerp ( Position camera, position of target in relation my desired position, percent between two position )
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);

            //position camera is equals my smoothed position;
            transform.position = smoothedPosition;
        }
    }
}
