﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CharacterController : MonoBehaviour
{
    [SerializeField]
    private float verticalSpeed, horizontalSpeed;

    private Rigidbody rb;
    private float verticalAxis, horizontalAxis;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        
        verticalAxis = Input.GetAxis("Vertical") * verticalSpeed;
        horizontalAxis = Input.GetAxis("Horizontal") * horizontalSpeed;

        rb.AddForce(Vector3.forward * verticalAxis);
        rb.AddForce(Vector3.right * horizontalAxis);
    }
    
}
